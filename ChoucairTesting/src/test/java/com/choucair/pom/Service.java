package com.choucair.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Service extends Base {

    By serviceLinkLocator = By.linkText("Servicios");
    By servicePageLocator = By.xpath
            ("//img[@src='https://www.choucairtesting.com/wp-content/uploads/2018/11/Banner-pag-interna.jpg.webp']");

    By capacityLinkLocator = By.linkText("Capacidades");
    By capacityPageLocator = By.xpath
            ("//img[@src='https://www.choucairtesting.com/wp-content/uploads/2018/10/investigacion.png']");

    By solutionPortfolio = By.linkText("Portafolio de Soluciones");


    By doneLinkLocator = By.partialLinkText("mo lo hacemos");

    By goToShow = By.xpath("//a[@class='go-top show']");

    By solutionPageLocator = By.xpath
            ("//img[@src='https://www.choucairtesting.com/wp-content/uploads/2018/10/presentacion.png']");

    By certification = By.linkText("Cursos y Certificaciones");

    By readMore = By.className("elementor-button-text");

    public Service(WebDriver driver) {
        super(driver);
    }

    public void testServices() throws InterruptedException {
        click(serviceLinkLocator);
        Thread.sleep(3000);
        System.out.println("Url= "+urlActual());


         if(isDisplayed(servicePageLocator)){
             System.out.println("Servicios:  ");

             if(isDisplayed(capacityPageLocator)){
                 Thread.sleep(3000);
/**
 * Capacidades
 */
                 System.out.println("--Capacidades--");
                 click(capacityLinkLocator);

                 Thread.sleep(3000);

                 click(serviceLinkLocator);
                 Thread.sleep(1000);

                 click(doneLinkLocator);
                 System.out.println("--Cómo lo hacemos--");
                 Thread.sleep(2000);

                 click(goToShow);
                 Thread.sleep(3000);

                 /**
                  * Portafolio de soluciones
                  */

                 if(isDisplayed(solutionPageLocator)) {
                     click(solutionPortfolio);
                     System.out.println("--Portafolio de soluciones--");
                     Thread.sleep(3000);

                     click(certification);
                     Thread.sleep(3000);

                     click(readMore);
                     Thread.sleep(4000);

                     click(By.linkText("http://www.choucairtesting.com"));
                     Thread.sleep(2000);



                 }
             }else
                 System.out.println("Page was not found");
         }else
             System.out.println("Page was not found");
    }


}

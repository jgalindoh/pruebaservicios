package com.choucair.pom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class TestService {
	
	private WebDriver driver;

	Service service;

	@Before
	public void setUp() throws Exception {

		service = new Service(driver);
		driver = service.chromeDriverConnection();
		driver.manage().window().maximize();
		service.urlPage("https://www.choucairtesting.com");
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() throws InterruptedException {

		service.testServices();

	}

}
